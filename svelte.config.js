import Path from "node:path";
import adapter from "svelte-adapter-bun";
import { vitePreprocess } from "@sveltejs/kit/vite";

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: vitePreprocess(),
  kit: {
    adapter: adapter(),
    alias: {
      $lib: Path.resolve("./src/lib"),
      $elements: Path.resolve("./src/lib/elements"),
      $parts: Path.resolve("./src/lib/parts"),
      $lang: Path.resolve("./lang"),
    },
  },
};

export default config;
