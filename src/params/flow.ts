import type { ParamMatcher } from "@sveltejs/kit";

export const match = ((param) => ["login", "registration"].includes(param)) satisfies ParamMatcher;
