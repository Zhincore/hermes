import { redirect } from "@sveltejs/kit";
import type { LayoutServerLoad } from "./$types";

export const load = (async (ev) => {
  const data = await ev.parent();
  if (!data.session) throw redirect(302, "/login");
}) satisfies LayoutServerLoad;
