import { rethrowKratosToSvelteError } from "$lib/server/Kratos";
import type { PageServerLoad } from "./$types";

export const load = (async (ev) => {
  return {
    sessions: ev.locals.kratos.getSessions().catch(rethrowKratosToSvelteError),
  };
}) satisfies PageServerLoad;
