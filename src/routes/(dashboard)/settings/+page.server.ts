import { rethrowKratosToSvelteError } from "$lib/server/Kratos";
import type { PageServerLoad } from "./$types";

export const load = (async (ev) => {
  const flowId = ev.url.searchParams.get("flow") ?? undefined;

  return {
    settings: ev.locals.kratos
      .getOrCreateFlow("settings", flowId, ev.url.searchParams)
      .catch(rethrowKratosToSvelteError),
  };
}) satisfies PageServerLoad;
