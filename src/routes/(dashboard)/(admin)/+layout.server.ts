import { error } from "@sveltejs/kit";
import type { LayoutServerLoad } from "./$types";

export const load = (async (ev) => {
  const data = await ev.parent();
  if (!data.isAdmin) throw error(403, "Forbidden");
}) satisfies LayoutServerLoad;
