import { env } from "$env/dynamic/public";
import { kratosAdmin } from "$lib/server/kratosAdmin";
import { fail, redirect } from "@sveltejs/kit";
import type { Actions } from "./$types";
import { rethrowKratosToSvelteError } from "$lib/server/Kratos";

export const actions = {
  default: async (ev) => {
    const data = await ev.request.formData();
    const isAdmin = data.has("isAdmin");
    const username = data.get("username");
    console.log(data.get("isAdmin"));

    if (!username) return fail(400, { username, missing: true });

    await kratosAdmin
      .createIdentity({
        schema_id: isAdmin ? env.PUBLIC_ADMIN_SCHEMA_ID : env.PUBLIC_DEFAULT_SCHEMA_ID,
        traits: {
          username,
        },
      })
      .catch(rethrowKratosToSvelteError);

    throw redirect(302, "/accounts");
  },
} satisfies Actions;
