import { rethrowKratosToSvelteError } from "$lib/server/Kratos";
import { kratosAdmin } from "$lib/server/kratosAdmin";
import type { PageServerLoad } from "./$types";

export const load = (async (ev) => {
  const recoverId = ev.url.searchParams.get("recover");

  return {
    identites: kratosAdmin.getIdentites().catch(rethrowKratosToSvelteError),
    recoveryLink: recoverId ? kratosAdmin.createRecoveryLink(recoverId).catch(rethrowKratosToSvelteError) : undefined,
  };
}) satisfies PageServerLoad;
