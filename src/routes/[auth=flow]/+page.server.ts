import { error, redirect } from "@sveltejs/kit";
import { KratosError } from "$lib/server/Kratos";
import { PUBLIC_REGISTRATION_DISBLED } from "$env/static/public";
import type { PageServerLoad } from "./$types";

export const load = ((ev) => {
  const flowId = ev.url.searchParams.get("flow") ?? undefined;
  const returnTo = ev.url.searchParams.get("return_to") ?? undefined;

  if (ev.params.auth == "registration" && PUBLIC_REGISTRATION_DISBLED == "true") {
    throw error(404, { message: "Registration is not available." });
  }

  return {
    title: ev.params.auth,
    flow: ev.locals.kratos
      .getOrCreateFlow(ev.params.auth as "login" | "registration", flowId, ev.url.searchParams)
      .catch((err) => {
        if (err instanceof KratosError) {
          if (err.id == "session_already_available") throw redirect(303, returnTo ?? "/");
          throw err.toSvelteError();
        }
        throw err;
      }),
  };
}) satisfies PageServerLoad;
