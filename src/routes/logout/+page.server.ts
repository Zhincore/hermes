import { redirect } from "@sveltejs/kit";
import type { PageServerLoad } from "./$types";

export const load = (async (ev) => {
  const result = await ev.locals.kratos.getLogoutUrl();

  throw redirect(302, result.logout_url);
}) satisfies PageServerLoad;
