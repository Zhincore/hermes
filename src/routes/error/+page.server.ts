import { error } from "@sveltejs/kit";
import type { PageServerLoad } from "./$types";

export const load = (async (ev) => {
  const data = await ev.locals.kratos.getFlowError(ev.url.searchParams.get("id") ?? "");
  throw error(data.error.code || 500, data.error);
}) satisfies PageServerLoad;
