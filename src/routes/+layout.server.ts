import { guessEventLocale } from "$lib/i18n";
import { isIdentityAdmin } from "$lib/kratosUtils";
import type { LayoutServerLoad } from "./$types";

export const load = (async (ev) => {
  const session = await ev.locals.kratos.getSession().catch();
  return {
    session,
    isAdmin: session ? isIdentityAdmin(session.identity) : false,
    locale: guessEventLocale(ev),
  };
}) satisfies LayoutServerLoad;
