import { initI18n } from "$lib/i18n";
import type { LayoutLoad } from "./$types";
import "$lib/utils";

export const load: LayoutLoad = async (ev) => {
  await initI18n(ev.data.locale);
  return ev.data;
};
