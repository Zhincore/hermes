declare module "*.yaml" {
  const data: object;
  export default data;
}

declare module "*.yml" {
  const data: object;
  export default data;
}

declare module "svelte-fa/src/fa.svelte" {
  export { Fa as default } from "svelte-fa";
}
