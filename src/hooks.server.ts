import { redirect, type Handle, type HandleFetch } from "@sveltejs/kit";
import { KRATOS_PUBLIC_URL } from "$env/static/private";
import { Kratos } from "$lib/server/Kratos";
import { dev } from "$app/environment";
import { createAdminAccount } from "$lib/server/adminAccount";

// Create default account on boot
createAdminAccount().catch(console.error);

export const handle = (async ({ event, resolve }) => {
  if (dev && event.url.hostname == "localhost") {
    // In dev mode, make sure use ://127.0.0.1 instead of ://localhost
    const url = new URL(event.url);
    url.hostname = "127.0.0.1";
    throw redirect(302, url.href);
  }

  // Create Kratos client
  event.locals.kratos = new Kratos(event);

  return resolve(event, {
    transformPageChunk: ({ html }) => html.replace("%lang%", event.params.lang ?? "en"),
  });
}) satisfies Handle;

export const handleFetch = (async ({ event, request, fetch }) => {
  const cookie = event.request.headers.get("cookie");
  if (cookie && request.url.startsWith(KRATOS_PUBLIC_URL)) {
    request.headers.set("cookie", cookie);
  }

  return fetch(request);
}) satisfies HandleFetch;
