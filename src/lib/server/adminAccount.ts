import { env } from "$env/dynamic/private";
import { env as publicEnv } from "$env/dynamic/public";
import { kratosAdmin } from "./kratosAdmin";

export async function createAdminAccount() {
  if (!env.DEFAULT_USERNAME) return;

  const identities = await kratosAdmin.getIdentites(env.DEFAULT_USERNAME);

  // The user exists, we're done
  if (identities.length && env.RECOVER_DEFAULT != "true") return;

  let identity = identities[0];

  if (!identities.length) {
    // Otherwise create it
    identity = await kratosAdmin.createIdentity({
      schema_id: publicEnv.PUBLIC_ADMIN_SCHEMA_ID,
      state: "active",
      traits: {
        username: env.DEFAULT_USERNAME,
      },
    });
  }

  // Create recovery link
  const recovery = await kratosAdmin.createRecoveryLink(identity.id, "4h");

  console.log(
    "###########\n",
    "Use this recovery link to set password for default account:\n",
    recovery.recovery_link,
    "\n\n",
    "It expires at:",
    recovery.expires_at,
    "\n###########"
  );
}
