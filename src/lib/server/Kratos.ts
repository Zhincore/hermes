import type {
  ErrorGeneric,
  Session,
  LoginFlow,
  FlowError,
  GenericError,
  RegistrationFlow,
  LogoutFlow,
  SettingsFlow,
  VerificationFlow,
} from "@ory/kratos-client";
import { redirect, error as svelteError } from "@sveltejs/kit";
import type { RequestEvent } from "@sveltejs/kit";
import setCookie from "set-cookie-parser-es";
import { env } from "$env/dynamic/private";
import { dev } from "$app/environment";
import { joinUrl, type Overwrite } from "$lib/utils";

type ErrorGenericContent = ErrorGeneric["error"];
export interface KratosError extends ErrorGenericContent {} // eslint-disable-line @typescript-eslint/no-empty-interface

export function kratosToSvelteError(error: ErrorGenericContent, allowRedirect = true) {
  if (allowRedirect && error.details && "redirect_browser_to" in error.details) {
    return redirect(302, error.details.redirect_browser_to as string);
  }

  return svelteError(error.code || 500, error);
}

export function rethrowKratosToSvelteError(error: Error): never {
  if (error instanceof KratosError) throw error.toSvelteError();
  throw error;
}

export class KratosError extends Error {
  name = "KratosError";
  #content: ErrorGenericContent;

  constructor({ error }: { error: KratosError }) {
    super(error.message);

    Object.assign(this, error);
    this.#content = error;

    if (!dev) {
      this.debug = undefined;
    }
  }

  toSvelteError(allowRedirect?: boolean) {
    return kratosToSvelteError(this.#content, allowRedirect);
  }
}

export type FlowTypes = {
  registration: RegistrationFlow;
  login: LoginFlow;
  settings: SettingsFlow;
  verification: VerificationFlow;
};
export type FlowType = keyof FlowTypes;

/** Custom API wrapper for Kratos */
export class Kratos {
  constructor(private readonly ev: RequestEvent) {}

  /**
   * Make a request to Kratos
   */
  async fetchKratos<T>(path: string, init?: RequestInit): Promise<T> {
    const result = await this.ev.fetch(joinUrl(env.KRATOS_PUBLIC_URL, path), {
      ...init,
      headers: {
        Accept: "application/json",
        ...init?.headers,
      },
    });

    // Force save cookies even if different domain
    const cookie = result.headers.get("set-cookie");
    if (cookie) {
      for (const item of setCookie.parse(cookie, { decodeValues: true })) {
        this.ev.cookies.set(item.name ?? "", item.value, { secure: !dev, ...item, encode: (v) => v });
      }
    }

    if (!result.ok) throw new KratosError(await result.json());

    return result.json();
  }

  /**
   * Get an existing flow or create new one if it doesn't exist or is expired
   */
  async getOrCreateFlow<T extends FlowType>(type: T, id?: string, query?: URLSearchParams) {
    type R = FlowTypes[T];
    let flow: R | undefined = undefined;

    if (id) {
      // Try to load existing flow
      flow = await this.fetchKratos<R>(`/self-service/${type}/flows?id=${id}`).catch((err) => {
        if (err instanceof KratosError && (err.code == 404 || err.id == "self_service_flow_expired")) {
          // Flow doesn't exist, let the fallback run
          return undefined;
        }
        // Rethrow other error
        throw new Error("Failed to load existing flow", { cause: err });
      });
    }

    if (!flow) {
      // Fallback to creating a new flow
      flow = await this.fetchKratos<R>(`/self-service/${type}/browser?` + query);
    }

    return flow;
  }

  /**
   * Get error with given identifier
   * @param id Identifier of the error
   */
  async getFlowError(id: string) {
    return await this.fetchKratos<Overwrite<FlowError, { error: GenericError }>>("/self-service/errors?id=" + id);
  }

  /**
   * Gets Kratos session or undefined if it doesn't exist
   */
  async getSession() {
    const cookie = this.ev.cookies.get("ory_kratos_session");

    // Skip fetching if we don't have Kratos' cookie
    if (!cookie) return;

    return this.fetchKratos<Session>("/sessions/whoami").catch((err: KratosError) => {
      if (err.code != 401) throw err;
      return undefined;
    });
  }

  /**
   * Gets sessions of current user
   */
  async getSessions(_page = 1) {
    // TODO: find out how page query parameter works
    const cookie = this.ev.cookies.get("ory_kratos_session");

    // Skip fetching if we don't have Kratos' cookie
    if (!cookie) return;

    return this.fetchKratos<Session[]>("/sessions");
  }

  /**
   * Create URL that logs out the user
   */
  async getLogoutUrl() {
    return this.fetchKratos<LogoutFlow>("/self-service/logout/browser");
  }
}
