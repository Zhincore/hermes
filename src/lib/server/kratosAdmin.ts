import type {
  CreateIdentityBody,
  CreateRecoveryLinkForIdentityBody,
  Identity,
  RecoveryLinkForIdentity,
} from "@ory/kratos-client";
import { env } from "$env/dynamic/private";
import { joinUrl } from "$lib/utils";
import { KratosError } from "./Kratos";

export async function fetchKratosAdmin<T, R = unknown>(path: string, init?: RequestInit, body?: R): Promise<T> {
  const result = await fetch(joinUrl(env.KRATOS_PRIVATE_URL, path), {
    body: body ? JSON.stringify(body) : undefined,
    method: body ? "POST" : "GET",
    ...init,
    headers: {
      Accept: "application/json",
      ...(body ? { "Content-Type": "application/json" } : {}),
      ...init?.headers,
    },
  });

  if (!result.ok) throw new KratosError(await result.json());

  return result.json();
}

export const kratosAdmin = {
  /** Get all identites or the one corresponding to the given identifier (e.g. username) */
  async getIdentites(identifier?: string) {
    return fetchKratosAdmin<Identity[]>(
      "/admin/identities?" + (identifier ? "credentials_identifier=" + identifier : "")
    );
  },

  /** Get an identity */
  async getIdentity(identityId?: string) {
    return fetchKratosAdmin<Identity>("/admin/identities/" + identityId);
  },

  /** Create an identity and return it */
  async createIdentity(identity: CreateIdentityBody) {
    return fetchKratosAdmin<Identity, CreateIdentityBody>("/admin/identities", {}, identity);
  },

  /** Create a recovery link for identity with given id */
  async createRecoveryLink(identityId: string, expiresIn?: string) {
    return fetchKratosAdmin<RecoveryLinkForIdentity, CreateRecoveryLinkForIdentityBody>(
      "/admin/recovery/link",
      {},
      {
        identity_id: identityId,
        expires_in: expiresIn,
      }
    );
  },
};
