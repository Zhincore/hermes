// This file is adapted from https://github.com/ory/integrations/blob/main/src/ui/index.ts
import type {
  Identity,
  UiNode,
  UiNodeAnchorAttributes,
  UiNodeImageAttributes,
  UiNodeInputAttributes,
  UiNodeScriptAttributes,
  UiNodeTextAttributes,
  UiText,
} from "@ory/kratos-client";
import { derived } from "svelte/store";
import { format } from "svelte-i18n";
import { env } from "$env/dynamic/public";
import type { Overwrite } from "./utils";
import type { MessageFormatter } from "./i18n";

/** Utility function to decide whether given identity is using defined admin schema */
export function isIdentityAdmin(identity: Identity) {
  return identity.schema_id == env.PUBLIC_ADMIN_SCHEMA_ID;
}

export const kratosText = derived(
  format,
  (fmt) => (text: UiText, opts?: Parameters<MessageFormatter>[1]) =>
    fmt("kratos." + text.id, {
      default: text.text,
      ...opts,
      values: {
        ...text.context,
        ...opts?.values,
      },
    })
);

/**
 * A TypeScript type guard for nodes of the type <a>
 *
 * @param attrs
 */
export function isUiNodeAnchor(node: UiNode): node is UiNodeAnchor {
  return node.type === "a";
}
export type UiNodeAnchor = Overwrite<UiNode, { attributes: UiNodeAnchorAttributes }>;

/**
 * A TypeScript type guard for nodes of the type <img>
 *
 * @param attrs
 */
export function isUiNodeImage(node: UiNode): node is UiNodeImage {
  return node.type === "img";
}
export type UiNodeImage = Overwrite<UiNode, { attributes: UiNodeImageAttributes }>;

/**
 * A TypeScript type guard for nodes of the type <input>
 *
 * @param attrs
 */
export function isUiNodeInput(node: UiNode): node is UiNodeInput {
  return node.type === "input";
}
export type UiNodeInput = Overwrite<UiNode, { attributes: UiNodeInputAttributes }>;

/**
 * A TypeScript type guard for nodes of the type <span>{text}</span>
 *
 * @param attrs
 */
export function isUiNodeText(node: UiNode): node is UiNodeText {
  return node.type === "text";
}
export type UiNodeText = Overwrite<UiNode, { attributes: UiNodeTextAttributes }>;

/**
 * A TypeScript type guard for nodes of the type <script>
 *
 * @param attrs
 */
export function isUiNodeScript(node: UiNode): node is UiNodeScript {
  return node.type === "script";
}
export type UiNodeScript = Overwrite<UiNode, { attributes: UiNodeScriptAttributes }>;
