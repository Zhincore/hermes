import type { Readable } from "svelte/store";

export type ExtractReadable<T> = T extends Readable<infer P> ? P : never;

export type Overwrite<T, P> = Omit<T, keyof P> & P;

export function capitalize(str: string) {
  return str.length ? str[0].toUpperCase() + str.slice(1) : "";
}

export function title(str: string) {
  return str
    .split(" ")
    .map((v) => capitalize(v))
    .join(" ");
}

export function joinUrl(...segments: string[]) {
  const url = new URL(segments.join("/"));
  url.pathname = url.pathname.replace(/\/{2,}/g, "/");
  return url;
}

export function intersection<T>(a: T[], b: T[]) {
  return a.filter((v) => b.includes(v));
}
