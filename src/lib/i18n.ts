import Cookies from "js-cookie";
import { init, locale, register, waitLocale } from "svelte-i18n";
import type { format } from "svelte-i18n";
import type { ServerLoadEvent } from "@sveltejs/kit";
import { browser, building } from "$app/environment";
import { intersection, type ExtractReadable } from "./utils";

export { _, time, date, number } from "svelte-i18n";

export type MessageFormatter = ExtractReadable<typeof format>;

export const LOCALE_COOKIE = "zhin-lang";
export const DEFAULT_LOCALE = "en";
const LOCALE_COOKIE_OPTS: Cookies.CookieAttributes = { sameSite: "Strict", expires: 365 };

export const languages = {
  en: () => import("$lang/en.yaml"),
  // cs: () => import("$lang/cs.yaml"),
} satisfies Record<string, () => Promise<unknown>>;

export async function initI18n(initialLocale: string) {
  for (const [lang, loader] of Object.entries(languages)) register(lang, loader);

  init({
    fallbackLocale: DEFAULT_LOCALE,
    initialLocale,
  });

  if (browser) {
    locale.subscribe((v) => {
      if (!v) return;
      // update cookie
      if (v != window.navigator.language) {
        Cookies.set(LOCALE_COOKIE, v, LOCALE_COOKIE_OPTS);
      } else {
        Cookies.remove(LOCALE_COOKIE, LOCALE_COOKIE_OPTS);
      }
    });
  }

  return waitLocale();
}

export function matchLocale(locales: (string | undefined | null)[]): string | undefined {
  const _locales = locales.map((v) => v?.trim().split("-")[0].toLowerCase()).filter((v) => v);

  return intersection(_locales, Object.keys(languages))[0];
}

export function guessEventLocale(ev: ServerLoadEvent) {
  // If user has locale cookie, then it's decided
  let locale: string | undefined = matchLocale([ev.cookies.get(LOCALE_COOKIE)]);

  // else try to match accept-language header
  if (!locale && !building) {
    const header = ev.request.headers.get("accept-language");
    if (header) {
      locale = matchLocale(header.split(",").map((v) => v.split("=")[0]));
    }
  }

  // otherwise use default locale
  if (!locale) {
    locale = DEFAULT_LOCALE;
  }

  return locale;
}
