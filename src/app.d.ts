import type { Session } from "@ory/kratos-client";
import type { Kratos } from "$lib/server/Kratos";

// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
  namespace App {
    // interface Error {}
    interface Locals {
      kratos: Kratos;
    }
    interface PageData {
      title?: string;
      locale: string;
      session?: Session;
    }
    // interface Platform {}
  }
}

export {};
