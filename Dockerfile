FROM node:16

ENV NODE_ENV=production
WORKDIR /app

RUN npm install -g pnpm

# Files required by pnpm install
COPY [ "package.json", "pnpm-lock.yaml", "./" ]

RUN pnpm install --frozen-lockfile --prod

# Bundle app source
COPY . .

EXPOSE 3000
CMD [ "node", "server.js" ]
