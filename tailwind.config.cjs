/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/app.html", "./src/**/*.{svelte,ts}", "./node_modules/flowbite-svelte/dist/**/*.{html,js,svelte,ts}"],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        accent: "#af24ca",
        "accent-light": "#c32def",
        nearblack: "#111",
        zinc: {
          850: "#202022",
        },
      },
      transitionDuration: {
        DEFAULT: "250ms",
      },
    },
  },
  plugins: [require("flowbite/plugin")],
};
