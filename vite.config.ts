import { sveltekit } from "@sveltejs/kit/vite";
import type { UserConfig } from "vite";
import { uneval } from "devalue";
import YAML from "yaml";

const config: UserConfig = {
  plugins: [
    sveltekit(),
    {
      // My custom YAML plugin
      name: "yaml",
      transform(src, id) {
        if (!id.match(/\.ya?ml$/)) return;

        return { code: "export default " + uneval(YAML.parse(src)) + ";" };
      },
    },
  ],
  server: {
    host: "0.0.0.0",
    port: process.env.PORT ? parseInt(process.env.PORT) : 3000,
    fs: {
      // Allow serving files from one level up to the project root
      allow: [".."],
    },
  },
};

export default config;
